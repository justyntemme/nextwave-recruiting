---
####################### Banner #########################
banner:
  title : "Recruiting for developers By developers"
  image : "images/banner-art.svg"
  content : "We know how to hire the best developers, because we are developers"
  button:
    enable : true
    label : "Contact Us"
    link : "contact/"

##################### Feature ##########################
feature:
  enable : true
  title : "Something You Need To Know"
  feature_item:
    # feature item loop
    - name : "Code Review"
      icon : "fas fa-code"
      content : "Our candidates are hand selected and vetted with a thorough process, to ensure only the best developers are presented for placement."
      
    # feature item loop
    - name : "Perfect Placement Oriented"
      icon : "fas fa-object-group"
      content : "NextWave finds the perfect candidate for the role, rather than trying to place candidates to several roles."
      
    # feature item loop
    - name : "Quick Response Promise"
      icon : "fas fa-user-clock"
      content : "We promise to always be there, whenever you may need us."
      
    # feature item loop
    - name : "Passionate Developers"
      icon : "fas fa-heart"
      content : "Find engineers that are passionate about building."
      
    # feature item loop
    - name : "Fast turnaround"
      icon : "fas fa-tachometer-alt"
      content : "Need an engineer tomorrow? Our selection process ensures we have a pool of excellent engineers to select from"
      
    # feature item loop
    - name : "Thought leaders"
      icon : "fas fa-cloud"
      content : "Our engineers do more than build your applications, they build an environment in which all developers can grow"
      


######################### Service #####################
service:
  enable : true
  service_item:
    # service item loop
    - title : "In todays world, numbers matter"
      images:
      - "images/service-1.png"
      - "images/service-2.png"
      - "images/service-3.png"
      content : "You don't have time to swim through thousands of candidates, so we ensure to have prime candidates available at any moment."
      button:
        enable : false
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "The data tells the story"
      images:
      - "images/service-1.png"
      content : "We have placed engineers in top fortune 500 companies, and will continue to serve our clients with engineers that inspires your team."
      button:
        enable : false
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "A team of with a distributed skill set"
      images:
      - "images/service-2.png"
      content : "We specialize in backend engineers who write applications in Golang, as well as cloud native developers for any stack. We have engineers with experience in a wide range of tech stacks."
      button:
        enable : false
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "A company standing different from others"
      images:
      - "images/service-3.png"
      content : "NextWave ensure that our team members not only inspire your team, but drive other team members to success. "
      button:
        enable : false
        label : "Check it out"
        link : "#"
        
################### Screenshot ########################
screenshot:
  enable : true
  title : "Experience the best <br> with us"
  image : "images/screenshot.svg"

  

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Ready to get started?"
  image : "images/cta.svg"
  content : "Email eli@nextwavesolutions.io to get started in finding the perfect fit for your team"
  button:
    enable : false
    label : "Contact Us"
    link : "contact/"
---
